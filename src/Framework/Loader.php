<?php

namespace Framework;

class Loader
{

	/**
	 * Indicates if a loader has been registered.
	 *
	 * @var bool
	 */
	protected $registered = false;

	/**
	 * The singleton instance of the loader.
	 *
	 * @var \Framework\Loader
	 */
	protected static $instance;

	/**
	 * Get or create the singleton loader instance.
	 *
	 * @return \Framework\Loader
	 */
	public static function getInstance()
	{
		if ( is_null( static::$instance ) ) {
			return static::$instance = new static();
		}

		return static::$instance;
	}

	/**
	 * Load a class name if it is registered.
	 *
	 * @param  string $className
	 *
	 * @return bool|null
	 */
	public function load( $className )
	{
		$ds  = DIRECTORY_SEPARATOR;
		$dir = __DIR__ . '/../../../../../';

		// replace namespace separator with directory separator
		$className = str_replace( '\\', $ds, $className );

		// get full name of file containing the required class
		$file = "{$dir}{$ds}{$className}.php";

		// get file if it is readable
		if ( is_readable( $file ) ) {
			require_once $file;
		}
	}

	/**
	 * Register the loader on the auto-loader stack.
	 *
	 * @return void
	 */
	public function register()
	{
		if ( ! $this->registered ) {
			$this->prependToLoaderStack();

			$this->registered = true;
		}
	}

	/**
	 * Prepend the load method to the auto-loader stack.
	 *
	 * @return void
	 */
	protected function prependToLoaderStack()
	{
		spl_autoload_register( [ $this, 'load' ], true, true );
	}
}