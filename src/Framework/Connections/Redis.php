<?php

namespace Framework\Connections;

use Framework\Application;
use Framework\Singleton;
use Predis\Client;
use React\EventLoop\LoopInterface;

class Redis extends Singleton
{

	/**
	 * The singleton instance of the Redis.
	 *
	 * @var \Framework\Connections\Redis
	 */
	protected static $instance;

	private $app;

	/**
	 * Holder of all redis settings
	 *
	 * @var object
	 */
	protected $settings;

	/**
	 * Redis client
	 *
	 * @var object
	 */
	protected $client;

	/**
	 * Indicates if Redis has been initialized.
	 *
	 * @var object
	 */
	protected $initialized = false;

	/**
	 * Loop is necessary for async Redis
	 *
	 * @var object
	 */
	protected $loop;

	/**
	 * Factory for async Redis
	 *
	 * @var object
	 */
	protected $factory;

	/**
	 * Redis constructor.
	 *
	 * @param Application $app
	 */
	public function __construct( Application $app )
	{
		$this->app = $app;

		$this->settings = [ 'host'               => @$_ENV['REDIS_HOST'],
		                    'port'               => @$_ENV['REDIS_PORT'],
		                    'password'           => @$_ENV['REDIS_PASSWORD'],
		                    'read_write_timeout' => 0,
		                    'timeout'            => - 1
		];
	}

	/**
	 * Initializing Redis
	 *
	 * @return $this
	 */
	public function initialize()
	{
		if ( ! $this->initialized ) {
			$this->client = new Client(
				array_filter( $this->settings ) + [
					'scheme' => 'tcp'
				]
			);

			$this->initialized = true;
		}

		return $this;
	}

	/**
	 * Initializing Async Redis
	 *
	 * @param $loop
	 *
	 * @return $this
	 */
	public function initializeAsync( LoopInterface $loop )
	{
		if ( ! $this->initialized ) {
			$this->loop = $loop;

			$this->factory = new \Clue\React\Redis\Factory( $this->loop );

			$redisSettings = array(
				'password' => $this->settings['password'],
				'timeout'  => $this->settings['timeout']
			);

			$redisSettingsQuery = http_build_query( array_filter( $redisSettings ) );

			$this->client = $this->factory->createLazyClient(
				'redis://' . $this->settings['host'] . ':' . $this->settings['port'] . '?' . $redisSettingsQuery
			);

			$this->initialized = true;
		}

		return $this;
	}

	/**
	 * Return Redis Client
	 */
	public function getClient()
	{
		return $this->client;
	}
}