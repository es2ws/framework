<?php

namespace Framework;

class Singleton
{

	/**
	 * The singleton instance of the Singleton.
	 *
	 * @var \Framework\Singleton
	 */
	protected static $instance;

	/**
	 * Get or create the singleton for Singleton instance.
	 *
	 * @param Application $app
	 *
	 * @return Singleton
	 */
	public static function getInstance(Application $app = null)
	{
		if ( is_null( static::$instance ) ) {
			return static::$instance = new static($app);
		}

		return static::$instance;
	}
}