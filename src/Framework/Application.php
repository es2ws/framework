<?php

namespace Framework;

use Framework\Connections\Redis;
use Dotenv\Dotenv;
use Twig\Environment;
use Twig\Loader\FilesystemLoader;

class Application
{
	/**
	 * The base path.
	 *
	 * @var string
	 */
	protected $basePath;

	/**
	 * Base CLI path.
	 *
	 * @var string
	 */
	protected $cliPath = "App\\Console\\Commands\\";

	/**
	 * Template engine views path
	 *
	 * @var string
	 */
	protected $viewsPath = 'views';

	/**
	 * Connections holder.
	 *
	 * @var object
	 */
	private $connections = [];

	/**
	 * Template engine.
	 *
	 * @var object
	 */
	private $templateEngine;

	/**
	 * Create new Application instance.
	 *
	 * @param string|null $basePath
	 */
	public function __construct( $basePath = null )
	{
		if ( $basePath ) $this->setBasePath( $basePath );

		$dotenv = Dotenv::createImmutable($this->basePath);
		$dotenv->safeLoad();

		Loader::getInstance()->register();

		$this->connections[Redis::class] = Redis::getInstance($this);

		$templateEngineLoader = new FilesystemLoader($this->viewsPath);
		$this->templateEngine = new Environment($templateEngineLoader);
	}

	/**
	 * Set the base path for the application.
	 *
	 * @param  string $basePath
	 *
	 * @return Application
	 */
	public function setBasePath( $basePath ):Application
	{
		$this->basePath = rtrim( $basePath, '\/' );

		return $this;
	}

	/**
	 * Check if application is started from console
	 *
	 * @return bool
	 */
	public function isCli():bool
	{
		if( defined('STDIN') ) return true;
		if( empty($_SERVER['REMOTE_ADDR']) and !isset($_SERVER['HTTP_USER_AGENT']) and count($_SERVER['argv']) > 0)return true;
		return false;
	}

	/**
	 * Get path to specific cli class
	 *
	 * @param null $commandClass
	 *
	 * @return string
	 */
	public function getCliPath( $commandClass = null ):string {
		if($commandClass) return $this->cliPath.$commandClass;
		return $this->cliPath;
	}

	/**
	 * Return all or single connection from available connections
	 *
	 * @param null $connection
	 *
	 * @return object
	 */
	public function getConnections( $connection = null ):object {
		if($connection) return $this->connections[$connection];
		return $this->connections;
	}

	/**
	 * Rendering view with template engine
	 *
	 * @param $name
	 *
	 * @param array $context
	 *
	 * @return string
	 */
	public function render( $name, array $context = [] ):string {
		return $this->templateEngine->render($name, $context);
	}

	/**
	 * Rendering view with template engine and set header to be json
	 *
	 * @param $name
	 *
	 * @param array $context
	 *
	 * @return string
	 */
	public function renderAsJson( $name, array $context = [] ):string {
		header('Content-Type: application/json');
		return $this->render($name, $context);
	}
}