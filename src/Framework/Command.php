<?php

namespace Framework;

class Command
{
	/**
	 * Return name of the class
	 *
	 * @return string
	 */
	public static function getClassName():string
	{
		return substr(strrchr(__CLASS__, "\\"), 1);
	}
}